--Default
function shape()
  local aShape={
      color = "red";
      filled = true;

      setColor = function(self, c)
          self.color = c;
      end;
      getColor = function(self)
        return self.color;
      end;
      setFilled = function(self, f)
          self.filled = f;
      end;
      isFilled = function(self)
        return self.filled;
      end;

      toString = function(self)
          print("Color =",self.color," Filled =",self.filled);
      end;
      classType = function()
        return "I am a Shape";
      end;
  }
  return aShape;
end
-- Input
function newShape(c, f)
  local bShape={
      color = c;
      filled = f;

      setColor = function(self, c)
          self.color = c;
      end;
      setFilled = function(self, f)
          self.filled = f;
      end;
      getColor = function(self)
        return self.color;
      end;
      isFilled = function(self)
        return self.filled;
      end;

      toString = function(self)
          print("Color =",self.color," Filled =",self.filled);
      end;
      classType = function()
        return "I am a Shape";
      end;
  }
  return bShape;
end

triangle = newShape("green", false);
triangle:toString();
triangle:setColor("blue");
triangle:toString();
print(triangle:classType());

---// Default Circle
function circle()
    local cShape = shape();
    cShape.radius = 1.0
    cShape.__index = cShape; ---// self
    cShape.setRadius = function(self, r)
        self.radius = r;
    end;
    cShape.getArea = function(self)
      return (self.radius * self.radius)
    end;
    cShape.getPerimeter = function(self)
      return (self.radius * 2 * 3.14)
    end;
    cShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Radius = ", self.radius);
    end;
    cShape.classType = function()
       return "I am a Circle and", shape().classType();
      end;
    return cShape;
end
---// Radius Circle
function radiusCircle(radius)
    local cShape = shape();
    cShape.radius = radius
    cShape.__index = cShape; ---// self
    cShape.setRadius = function(self, r)
        self.radius = r;
    end;
    cShape.getArea = function(self)
      return (self.radius * self.radius)
    end;
    cShape.getPerimeter = function(self)
      return (self.radius * 2 * 3.14)
    end;
    cShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Radius = ", self.radius);
    end;
    cShape.classType = function()
        return "I am a Circle and", shape().classType();
      end;
    return cShape;
end
---// Input Circle
function completeCircle(c, f, radius)
    local cShape = newShape(c, f);
    cShape.radius = radius
    cShape.__index = cShape; ---// self
    cShape.setRadius = function(self, r)
        self.radius = r;
    end;
    cShape.getArea = function(self)
      return (self.radius * self.radius)
    end;
    cShape.getPerimeter = function(self)
      return (self.radius * 2 * 3.14)
    end;
    cShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Radius = ", self.radius);
    end;
    cShape.classType = function()
        return "I am a Circle and", shape().classType();
      end;
    return cShape;
end
print()
print()
goldRing = completeCircle("gold", false, 2.0);
goldRing:toString();
goldRing:setRadius(3.0);
goldRing:toString();
print("Area", goldRing:getArea());
print("Perimeter", goldRing:getPerimeter());
print(goldRing:classType());

--Default Rectangle
function rectangle()
    local rShape = shape();
    rShape.width = 1.0
    rShape.length = 1.0
    rShape.__index = rShape; ---// self
    rShape.setWidth = function(self, w)
        self.width = w;
    end;
    rShape.setLength = function(self, l)
        self.length = l;
    end;
    rShape.getArea = function(self)
      return (self.width * self.length)
    end;
    rShape.getPerimeter = function(self)
      return ((self.width * 2) + (self.length * 2))
    end;
    rShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Width = ", self.width, "Length = ", self.length);
    end;
    rShape.classType = function()
        return "I am a Rectangle and", shape().classType();
      end;
    return rShape;
end
--Length/Width Rectangle
function newRectangle(l,w)
    local rShape = shape();
    rShape.width = w
    rShape.length = l
    rShape.__index = rShape; ---// self
    rShape.setWidth = function(self, w)
        self.width = w;
    end;
    rShape.setLength = function(self, l)
        self.length = l;
    end;
    rShape.getArea = function(self)
      return (self.width * self.length)
    end;
    rShape.getPerimeter = function(self)
      return ((self.width * 2) + (self.length * 2))
    end;
    rShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Width = ", self.width, "Length = ", self.length);
    end;
    rShape.classType = function()
        return "I am a Rectangle and", shape().classType();
      end;
    return rShape;
end
--Complete Rectangle
function completeRectangle(c, f, l, w)
    local rShape = newShape(c, f);
    rShape.width = w
    rShape.length = l
    rShape.__index = rShape; ---// self
    rShape.setWidth = function(self, w)
        self.width = w;
    end;
    rShape.setLength = function(self, l)
        self.length = l;
    end;
    rShape.getArea = function(self)
      return (self.width * self.length)
    end;
    rShape.getPerimeter = function(self)
      return ((self.width * 2) + (self.length * 2))
    end;
    rShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Width = ", self.width, "Length = ", self.length);
    end;
    rShape.classType = function()
        return "I am a Rectangle and", shape().classType();
      end;
    return rShape;
end
print()
print()
field = completeRectangle("green", true, 100.0, 20.0);
field:toString();
field:setWidth(50.0);
field:toString();
print("Area", field:getArea());
print("Perimeter", field:getPerimeter());
print(field:classType());

--Default Square
function square()
    local sShape = rectangle();
    sShape.side = 1.0
    sShape.__index = sShape; ---// self
    sShape.setSide = function(self, s)
        self.side = s;
    end;
    sShape.getArea = function(self)
      return (self.side * self.side)
    end;
    sShape.getPerimeter = function(self)
      return (self.side * 4)
    end;
    sShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Side = ", self.side);
    end;
    sShape.classType = function()
        return "I am a Square and", rectangle().classType();
      end;
    return sShape;
end
--Side Square
function newSquare(s)
    local sShape = rectangle();
    sShape.side = s
    sShape.__index = sShape; ---// self
    sShape.setSide = function(self, s)
        self.side = s;
    end;
    sShape.getArea = function(self)
      return (self.side * self.side)
    end;
    sShape.getPerimeter = function(self)
      return (self.side * 4)
    end;
    sShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Side = ", self.side);
    end;
    sShape.classType = function()
        return "I am a Square and", rectangle().classType();
      end;
    return sShape;
end
--Complete Square
function completeSquare(c, f, s)
    local sShape = newShape(c, f);
    sShape.side = s
    sShape.__index = sShape; ---// self
    sShape.setSide = function(self, s)
        self.side = s;
    end;
    sShape.getArea = function(self)
      return (self.side * self.side)
    end;
    sShape.getPerimeter = function(self)
      return (self.side * 4)
    end;
    sShape.toString = function(self)
        print("Color =",self.color," Filled =",self.filled, "Side = ", self.side);
    end;
    sShape.classType = function()
        return "I am a Square and", rectangle().classType();
      end;
    return sShape;
end
print()
print()
beThereOr = completeSquare("black", false, 6.0);
beThereOr:toString();
beThereOr:setSide(7.0);
beThereOr:toString();
print("Area", beThereOr:getArea());
print("Perimeter", beThereOr:getPerimeter());
print(beThereOr:classType());